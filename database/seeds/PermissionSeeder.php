<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();

        foreach (config('permission.permissions') as $key => $value) {
            $permissions = Permission::create([
                'name' => $key,
                'display_name' => '',
                'group' => 0
            ]);
            foreach ($value as  $keyPermission => $permission) {
                Permission::create([
                    'name' => $key. '-' .$permission,
                    'display_name' => '',
                    'group' => $permissions->id,
                ]);
            }
        }
    }
}
