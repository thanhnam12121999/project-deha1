<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Admin', 'username' => 'admin', 'email' => 'test1@gmail.com' , 'phone' => 0705647516, 'password' => Hash::make(1), 'is_admin' => 1],
            ['name' => 'Guess', 'username' => 'user1', 'email' => 'test2@gmail.com', 'phone' => 0707777516, 'password' => Hash::make(1), 'is_admin' => 0],
        ]);
    }
}
