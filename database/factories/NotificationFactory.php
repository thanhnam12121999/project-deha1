<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Notification;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Notification::class, function (Faker $faker) {
    return [
        'id' => (string) Str::uuid(),
        'type' => 'App\Notifications\User\UserNotification',
        'notifiable_type' => 'App\User',
        'notifiable_id' => 1,
        'data' => json_encode(["content" => $faker->name()] )
    ];
});
