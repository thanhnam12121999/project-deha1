<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('/', [
            'as' => 'users.index',
            'uses' => 'Backend\UserController@index',
            'middleware' => 'permission:user-list',
        ]);
        Route::get('show/{id}', [
            'as' => 'users.show',
            'uses' => 'Backend\UserController@show',
            'middleware' => 'permission:user-show',
        ]);
        Route::post('store', [
            'as' => 'users.store',
            'uses' => 'Backend\UserController@store',
            'middleware' => 'permission:user-add',
        ]);
        Route::put('update/{id}', [
            'as' => 'users.update',
            'uses' => 'Backend\UserController@update',
            'middleware' => 'permission:user-edit',
        ]);
        Route::delete('destroy/{id}', [
            'as' => 'users.destroy',
            'uses' => 'Backend\UserController@destroy',
            'middleware' => 'permission:user-delete',
        ]);
        Route::get('list', [
            'as' => 'users.list',
            'uses' => 'Backend\UserController@getListUser',
            'middleware' => 'permission:user-list',
        ]);
    });
});
