<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function () {
    Route::prefix('categories')->group(function () {
        Route::get('/', [
            'as' => 'categories.index',
            'uses' => 'Backend\CategoryController@index',
            'middleware' => 'permission:category-list',//
        ]);
        Route::post('store', [
            'as' => 'categories.store',
            'uses' => 'Backend\CategoryController@store',
            'middleware' => 'permission:category-add',
        ]);
        Route::put('update/{id}', [
            'as' => 'categories.update',
            'uses' => 'Backend\CategoryController@update',
            'middleware' => 'permission:category-edit',
        ]);
        Route::get('show/{id}', [
            'as' => 'categories.show',
            'uses' => 'Backend\CategoryController@show',
            'middleware' => 'permission:category-show',
        ]);
        Route::delete('delete/{id}', [
            'as' => 'categories.delete',
            'uses' => 'Backend\CategoryController@destroy',
            'middleware' => 'permission:category-delete',
        ]);
        Route::get('list', [
            'as' => 'categories.list',
            'uses' => 'Backend\CategoryController@getListCategory',
            'middleware' => 'permission:category-list',
        ]);
        Route::get('list-select', [
            'as' => 'categories.list_select',
            'uses' => 'Backend\CategoryController@getListSelectHtml',
        ]);
    });
});
