<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function () {
    Route::prefix('permissions')->group(function () {
        Route::get('/', [
            'as' => 'permissions.index',
            'uses' => 'Backend\PermissionController@index',
            'middleware' => 'permission:permission-list',
        ]);
        Route::post('store', [
            'as' => 'permissions.store',
            'uses' => 'Backend\PermissionController@store',
            'middleware' => 'permission:permission-add',
        ]);
    });
});
