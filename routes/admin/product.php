<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'locale'])->group(function () {
    Route::prefix('products')->group(function () {
        Route::get('/', [
            'as' => 'products.index',
            'uses' => 'Backend\ProductController@index',
            'middleware' => 'permission:product-list',
        ]);
        Route::get('list', [
            'as' => 'products.list',
            'uses' => 'Backend\ProductController@getlistProduct',
            'middleware' => 'permission:product-list',
        ]);
        Route::get('/{id}', [
            'as' => 'products.show',
            'uses' => 'Backend\ProductController@show',
            'middleware' => 'permission:product-show',
        ]);
        Route::post('/', [
            'as' => 'products.store',
            'uses' => 'Backend\ProductController@store',
            'middleware' => 'permission:product-add',
        ]);
        Route::put('/{id}', [
            'as' => 'products.update',
            'uses' => 'Backend\ProductController@update',
            'middleware' => 'permission:product-edit',
        ]);
        Route::delete('/{id}', [
            'as' => 'products.destroy',
            'uses' => 'Backend\ProductController@destroy',
            'middleware' => 'permission:product-delete',
        ]);
        Route::post('images/{id}', [
            'as' => 'products.update_image',
            'uses' => 'Backend\ProductController@updateImages',
        ]);
        Route::delete('images/{image}', [
            'as' => 'products.delete_image',
            'uses' => 'Backend\ProductController@deleteImage',
        ]);
    });
});
