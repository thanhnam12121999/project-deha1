<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function () {
    Route::prefix('roles')->group(function () {
        Route::get('/', [
            'as' => 'roles.index',
            'uses' => 'Backend\RoleController@index',
            'middleware' => 'permission:role-list',
        ]);
        Route::get('show/{id}', [
            'as' => 'roles.show',
            'uses' => 'Backend\RoleController@show',
            'middleware' => 'permission:role-show',
        ]);
        Route::put('update/{id}', [
            'as' => 'roles.update',
            'uses' => 'Backend\RoleController@update',
            'middleware' => 'permission:role-edit',
        ]);
        Route::post('store', [
            'as' => 'roles.store',
            'uses' => 'Backend\RoleController@store',
            'middleware' => 'permission:role-add',
        ]);
        Route::delete('delete/{id}', [
            'as' => 'roles.delete',
            'uses' => 'Backend\RoleController@destroy',
            'middleware' => 'permission:role-delete',
        ]);
        Route::get('list', [
            'as' => 'roles.list',
            'uses' => 'Backend\RoleController@getListRole',
            'middleware' => 'permission:role-list',
        ]);
    });
});
