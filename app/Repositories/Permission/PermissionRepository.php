<?php

namespace App\Repositories\Permission;

use App\Models\Permission;
use App\Repositories\BaseRepository;

class PermissionRepository extends BaseRepository
{
    protected $permission;

    public function model()
    {
        return Permission::class;//
    }

    public function getAllParentPermission()
    {
        return $this->model->where('group', 0)->get();
    }

    public function getWithName($name)
    {
        return $this->model->where('name', '=', $name)->first();
    }   
}