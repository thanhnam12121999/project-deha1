<?php

namespace App\Repositories\Category;

use App\Models\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{

    public function model()
    {
        return Category::class;
    }

    public function getWithPaginate($quantity)
    {
        return $this->model->paginate($quantity);
    }

    public function search($name)
    {
        return $this->model
            ->withName($name)
            ->paginate($quantity = 10);
    } //search

    public function update($request, $id)
    {
        $category = $this->findById($id);
        $dataUpdate = [
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'slug' => $category->slug
        ];
        return $this->updateById($id, $dataUpdate);
    }

    public function delete($id)
    {
        return $this->deleteById($id);
    }

    public function getWithParent() //
    {
        return $this->model->withParentID()->with('childs')->get();
    }
}
