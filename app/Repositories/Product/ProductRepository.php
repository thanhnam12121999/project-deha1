<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BaseRepository;
use App\Traits\HandleImages;

class ProductRepository extends BaseRepository
{
    use HandleImages;

    public function model()
    {
        return Product::class;
    }

    public function getWithPaginate($quantity)
    {
        return $this->model->paginate($quantity);
    }

    public function getById($id)
    {
        return $this->findById($id);
    }

    public function delete($id)
    {
        return $this->deleteById($id);
    }

    public function search($dataSearch)
    {
        return $this->model
            ->withName($dataSearch['name']) 
            ->withCategory($dataSearch['category'])
            ->with(['category', 'firstImage'])->paginate($quantity = 10);
    }
}
