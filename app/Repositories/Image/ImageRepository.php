<?php

namespace App\Repositories\Image;

use App\Models\Image;
use App\Repositories\BaseRepository;
use App\Traits\HandleImages;

class ImageRepository extends BaseRepository
{
    use HandleImages;

    public function model()
    {
        return Image::class; 
    }
    
    public function deleteImages($listID)
    {
        return $this->model->whereIn('id', $listID)->delete();
    }
}
