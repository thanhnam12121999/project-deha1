<?php

namespace App\Repositories\User;

use App\Http\Resources\UserResource;
use App\Repositories\BaseRepository;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserRepository extends BaseRepository
{

    public function model()
    {
        return User::class; //model instance
    }

    public function getUserPaginate($quantity)
    {
        return $this->model->paginate($quantity);
    }

    public function search($name)
    {
        return  $this->model
            ->withName($name)
            ->with('roles')
            ->paginate($quantity = 10);
         
    }

    public function getById($id)
    {
        $user = $this->model->with('roles')->find($id);
        return new UserResource($user);
    }

    public function getLatestNotification()
    {
        if (!Auth::user()) {
            return null;
        }
        return Auth::user()->notifications()->paginate($quantity = 4);
    }
    public function delete($id)
    {
        return $this->deleteById($id);
    }

}
