<?php

namespace App\Repositories\Role;

use App\Http\Resources\RoleResource;
use App\Models\Role;
use App\Repositories\BaseRepository;
use Throwable;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function getWithPaginate($quantity)
    {
        return $this->model->paginate($quantity);
    }

    public function search($name)
    {
        return $this->model
            ->withName($name)
            ->paginate($quantity = 10);
    }

    public function update($request, $id)
    {
        $dataUpdate  = [
            "name" => $request->name,
            "display_name" => $request->display_name,
        ];
        $this->updateById($id, $dataUpdate);
        return $this->findById($id)
            ->permissions()
            ->sync($request->permission_id);

    }

    public function store($request)
    {
        $dataStore = [
            'name' => $request->name,
            'display_name' => $request->display_name,
        ];
        $rolePermission = $this->create($dataStore);
        return $rolePermission->permissions()->attach($request->permission_id);
    }
    
}
