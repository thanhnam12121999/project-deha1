<?php

namespace App\Repositories;

use Illuminate\Http\Response;

abstract class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct()
    {
        $this->makeModel();
    }

    abstract public function model();

    public function makeModel()
    {
        $this->model = app()->make(
            $this->model()
        );
    }

    public function all()
    {
        return $this->model->all();
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function updateById($id, array $attributes = [])
    {
        $model = $this->findById($id);//
        $model->update($attributes);
        return $model;
    }

    public function deleteById($id)
    {
        $model = $this->findById($id);
        $model->delete();
        return true;
    }

    public function responseStatus($message, $data = [], $statusCode = Response::HTTP_OK)
    {
        return array_merge([
            'statusCode' => $statusCode,
            'message' => $message
        ], $data);
    }
}
