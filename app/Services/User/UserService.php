<?php

namespace App\Services\User;

use App\Notifications\User\UserNotification;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Hash;
use App\Services\BaseService;

class UserService extends BaseService
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function fillDataStoreUser($request)
    {
        return [
            'name' => $request->name,
            'username' => $request->username,
            'phone' => $request->phone,
            'password' => Hash::make($request->password)
        ];
    }

    public function fillDataUserUpdate($request, $id)
    {
        $user = $this->userRepository->findById($id);
        if (!$request->password === null) {
            $password = Hash::make($request->password);// format mutator 
        } else {
            $password = $user->password;//
        }
        return [
            'name' => $request->name,
            'username' => $request->username,
            'phone' => $request->phone,
            'password' => $password,//
        ];
    }

    public function create($request)
    {
        $dataCreate = $this->fillDataStoreUser($request);
        $user = $this->userRepository->create($dataCreate);
        $user->roles()->attach($request->role_id);//function
        $this->createNewUserNotification($user->username);
        return true;
    }

    public function createNewUserNotification($username)
    {
        $user = $this->userRepository->findById($admin = 1);
        $content = 'New user registered (' . $username . ')';
        $dataNotification = [
            'title' => 'New User',
            'content' => $content,
        ];
        $user->notify(new UserNotification($dataNotification));
        return true;
    }

    public function update($request, $id)
    {
        $dataUpdate = $this->fillDataUserUpdate($request, $id);
        $user = $this->userRepository->updateById($id, $dataUpdate);
        $user->roles()->sync($request->role_id);
        return true;
    }

    public function getById($id)
    {
        return $this->userRepository->getById($id);
    }

    public function search($request)
    {
        $nameUser = $request->search ?? null;
        return $this->userRepository->search($nameUser);
    }

    public function getLatestNotification()
    {
        return $this->userRepository->getLatestNotification();
    }

    public function delete($id)
    {
        $this->userRepository->delete($id);
        return true;
    }
}
