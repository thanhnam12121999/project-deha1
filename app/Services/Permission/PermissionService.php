<?php

namespace App\Services\Permission;

use App\Http\Resources\PermissionResource;
use App\Models\Permission;
use App\Repositories\Permission\PermissionRepository;
use Illuminate\Support\Facades\DB;
use App\Services\BaseService;

class PermissionService extends BaseService
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getAll()
    {
        return $this->permissionRepository->all();
    }

    public function getAllParentPermission()
    {
        return PermissionResource::collection($this->permissionRepository->getAllParentPermission());
    }

    public function create($request)
    {
        $permission = $this->permissionRepository->getWithName($request->group);
        if (is_null($permission)) {
            $this->storeParentNotExist($request);
        } else {
            $this->storeParentExist($request, $permission->id);
        }
        return true;
    }

    public function storeParentNotExist($request)
    {
        $dataCreate = [
            'name' => $request->group,
            'display_name' => '',
            'group' => 0,
        ];
        $permission = $this->permissionRepository->create($dataCreate);
        foreach ($request->childs as  $value) {
            $name = $request->group . '-' . $value;
            $permissions = $this->storeChildPermission($name, $permission->id);
        }
    }

    public function storeParentExist($request, $id)
    {
        foreach ($request->childs as  $value) {
            $name = $request->group . '-' . $value;
            $permission = $this->permissionRepository->getWithName($name);
            if (is_null($permission)) {
                $this->storeChildPermission($name, $id);
            }
        }
    }

    public function storeChildPermission($name, $id)
    {
        $dataCreate = [
            'name' => $name,
            'display_name' => '',
            'group' => $id,
        ];
        $this->permissionRepository->create($dataCreate);
    }
}
