<?php

namespace App\Services\Image;

use App\Repositories\Image\ImageRepository;
use App\Services\BaseService;
use App\Traits\HandleImages;

class ImageService extends BaseService
{
    use HandleImages;

    protected $imageRepository;
    protected $mutilpleImageRequest = 'multiple-image';

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function deleteImages($imageListDelete)
    {
        $listID = array_keys($imageListDelete);
        $this->imageRepository->deleteImages($listID);
        $listNameImage = array_values($imageListDelete);
        foreach ($listNameImage as $name) {
            $this->deleteImage($name);
        }
    }

    public function saveMutilpleImageFile($request, $object)
    {
        if ($request->has('imageListDelete')) {
            $this->deleteImages($request->imageListDelete);
        }
        $listName = $this->storeFileToFolder($request, $this->mutilpleImageRequest);
        if($listName) $object->images()->createMany($listName);
    }
}
