<?php

namespace App\Services\Product;

use App\Repositories\Product\ProductRepository;
use App\Services\BaseService;
use App\Traits\HandleImages;

class ProductService extends BaseService
{
    use HandleImages;

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function fillDataProduct($request)
    {
        $userID = Auth()->user()->id;
        return [
            'name' => $request->name,
            'category_id' => $request->category_id,
            'user_id' => $userID,
            'description' => $request->description,
            'price' => $request->price,
            'status' => $request->status
        ];
    }

    public function store($request)
    {
        $dataCreate = $this->fillDataProduct($request);
        return $this->productRepository->create($dataCreate);
    }

    public function update($request, $id)
    {
        $dataUpdate = $this->fillDataProduct($request);
        return $this->productRepository->updateById($id, $dataUpdate);
    }

    public function getById($id)
    {
        return $this->productRepository->getById($id);
    }

    public function delete($id)
    {
        return $this->productRepository->delete($id);
    }

    public function search($request)
    {
        $dataSearch = [
            'name' => $request->search ?? null,
            'category' => $request->category ?? null
        ];
        return $this->productRepository->search($dataSearch);
    }
}
