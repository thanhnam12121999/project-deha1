<?php

namespace App\Services\Role;

use App\Repositories\Role\RoleRepository;
use App\Services\BaseService;

class RoleService extends BaseService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getALL()
    {
        return $this->roleRepository->all();
    }

    public function create($request)
    {
        $role = $this->roleRepository->store($request);
        return $role;
    }

    public function getListRole($request)
    {
        $nameRole = $request->search ?? null;
        return $this->roleRepository->search($nameRole);
    }

    public function update($request, $id)
    {
        return $this->roleRepository->update($request, $id);
    }

    public function delete($id)
    {
        $role = $this->roleRepository->findById($id);
        $this->roleRepository->deleteById($id);
        $role->permissions()->detach();
        return true;
    }

    public function getById($id)
    {
        return $this->roleRepository->findById($id);
    }
}
