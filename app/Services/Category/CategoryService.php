<?php

namespace App\Services\Category;

use App\Repositories\Category\CategoryRepository;
use App\Services\BaseService;
use Throwable;

class CategoryService extends BaseService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getAll() //
    {
        return $this->categoryRepository->all();//controller
    } //viewShare compose

    public function getById($id)
    {
        return $this->categoryRepository->findById($id); //
    }

    public function getWithParent()
    {
        return $this->categoryRepository->getWithParent();
    }

    public function store($request) //
    {
        $dataCreate = [
            'name' => $request->name, //
            'parent_id' => $request->parent_id,
            'slug' => '',
        ];
        return $this->categoryRepository->create($dataCreate);
    }

    public function update($request, $id)
    {
        return $this->categoryRepository->update($request, $id);
    }

    public function search($request)
    {
        $name = $request->search ?? null;
        return $this->categoryRepository->search($name);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }
}
