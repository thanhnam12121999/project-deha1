<?php

namespace App\Services;

use Illuminate\Http\Response;

class BaseService
{
    public function responseStatus($message, $data = [], $statusCode = Response::HTTP_OK)
    {
        return array_merge([
            'statusCode' => $statusCode,
            'message' => $message
        ], $data);
    }
}