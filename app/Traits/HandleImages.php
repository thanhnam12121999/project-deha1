<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;

/**
 * 
 */
trait HandleImages
{

    public $pathImage = 'assets/images/products/';

    public function getNameFile($image)
    {
        return $image->getClientOriginalName();
    }

    public function storeFileToFolder($request, $mutilpleImageRequest)
    {
        if ($request->hasFile($mutilpleImageRequest)) {
            $data = [];
            $images = $request->file($mutilpleImageRequest);
            foreach ($images as $index => $image) {
                $fileName = rand() . '-' . $this->getNameFile($image);
                $this->moveImage($image, $fileName);
                $data[] = ['file' => $fileName];
            }
            return $data;
        } else {
            return false;
        }
    }

    public function deleteImage($imageName)
    {
        
        File::delete($this->pathImage . $imageName);
        return true;
    }

    public function moveImage($image, $imageName)
    {
        $image->move(public_path($this->pathImage), $imageName);
    }

    // public function updateFileImage($request, $avatar, $avatarRequest)
    // {
    //     if ($request->hasFile($avatarRequest)) {
    //         $image = $request->file($avatarRequest);
    //         $imageName = $this->getNameFile($image);
    //         $this->deleteImage($avatar);
    //         $this->moveImage($image, $imageName);
    //     } else {
    //         $imageName  = $avatar;
    //     }
    //     return $imageName;
    // }

    // public function creatFileImage($request, $avatarRequest)
    // {
    //     $avatar = $request->file($avatarRequest);
    //     $newName  = rand() . '-' . $this->getNameFile($avatar);
    //     $this->moveImage($avatar, $newName);
    //     return $newName;
    // }
}
