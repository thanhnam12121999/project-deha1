<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'role_id', 'permission_id');
    }

    public function scopeWithName($query, $name)
    {
        if ($name) return $query->where('name', 'LIKE', "%" . $name . "%");
    }

    public function hasPermission($permissionName)
    {
        return $this->permissions()->where('name', $permissionName)->count() > 0;
    }
}
