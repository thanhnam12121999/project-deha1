<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $guarded = [];

    public function permissionsChildrent() 
    {
        return $this->hasMany(Permission::class, 'group');
    }

    public function scopeWithGroup($query)
    {
        return $query->where('group', '!=', 0);
    }
}
