<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "name",
        "category_id",
        "user_id",
        "description",
        "price",
        "status"
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function firstImage()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function scopeWithName($query, $name)
    {
        if ($name) return $query->where('name', 'LIKE', "%" . $name . "%");
    }

    public function scopeWithCategory($query, $category)
    {
        if ($category) return $query->where('category_id', $category);
    }

}
