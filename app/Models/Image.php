<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['file'];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function scopeWithImageableID($query, $id)
    {
        if($id) return $query->where('imageable_id', $id);
    }
}
