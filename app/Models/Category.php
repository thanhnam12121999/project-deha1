<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = ['name', 'parent_id', 'slug'];

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'LIKE', "%" . $name . "%");
    }

    public function scopeWithParentID($query)
    {
        return $query->where('parent_id', 0);
    }
}
