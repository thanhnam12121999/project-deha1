<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Category\CategoryRequest;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService  = $categoryService;
    }

    public function index()
    {
        $categories = CategoryResource::collection($this->categoryService->getAll()); //
        return view('backend.category.index', compact('categories'));
    }

    public function show(int $id)
    {
        $category = $this->categoryService->getById($id);
        return response()->json([
            'data' => $category,
            'message' => 'Get category success.'
        ], Response::HTTP_OK);
    }

    public function update(CategoryRequest $request, int $id)
    {
        $category = $this->categoryService->update($request, $id);
        return response()->json([
            'data' => $category,
            'message' => 'Update category success'
        ], Response::HTTP_OK);
    }

    public function store(CategoryRequest $request)
    {
        $category = $this->categoryService->store($request);
        return response()->json([
            'data' => $category,
            'message' => 'Create category success'
        ], Response::HTTP_OK);
    }

    public function destroy(int $id)
    {
        $this->categoryService->delete($id);
        return response()->json([
            'data' => true,
            'message' => 'Delete Category success.'
        ], Response::HTTP_OK);
    }

    public function getListCategory(Request $request) //list
    {
        $catagories =  $this->categoryService->search($request);
        return view('backend.category.list', compact('catagories'));
    }

    public function getListSelectHtml()
    {
        $categories = $this->categoryService->getWithParent();
        return view('backend.category.main-categories-select', compact('categories'));
    }

}
