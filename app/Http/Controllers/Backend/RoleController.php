<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Role\RoleStoreRequest;
use App\Http\Requests\Role\RoleUpdateRequest;
use App\Http\Resources\RoleResource;
use App\Services\Permission\PermissionService;
use App\Services\Role\RoleService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    protected $roleService;
    protected $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function index()
    {
        $permissions = RoleResource::collection($this->permissionService->getAllParentPermission());
        return view('backend.role.index', compact('permissions'));
    }

    public function show($id)
    {
        $role = new RoleResource($this->roleService->getById($id));
        return response()->json([
            'data' => $role,
            'message' => 'Get role success.'
        ], Response::HTTP_OK);
    }

    public function update(RoleUpdateRequest $request, $id)
    {
        $role = $this->roleService->update($request, $id);
        return response()->json([
            'data' => $role,
            'message' => 'Update role success.'
        ], Response::HTTP_OK);
    }

    public function store(RoleStoreRequest $request)
    {
        $role = $this->roleService->create($request);
        return response()->json([
            'data' => $role,
            'message' => 'Store role success.'
        ], Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $this->roleService->delete($id);
        return response()->json([
            'data' => true,
            'message' => 'Delete role success.'
        ], Response::HTTP_OK);
    }

    public function getListRole(Request $request)
    {
        $roles = RoleResource::collection($this->roleService->getListRole($request));
        return view('backend.role.list', compact('roles'));
    }
}
