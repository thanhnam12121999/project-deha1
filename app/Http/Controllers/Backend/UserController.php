<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Role\RoleService;
use Illuminate\Http\Response;

class UserController extends Controller
{
    protected $userServices;
    protected $roleService;

    public function __construct(UserService $userServices, RoleService $roleService)
    {
        $this->userServices = $userServices;
        $this->roleService = $roleService;
    }

    public function index()
    {
        $categories = $this->roleService->getALL();
        return view('backend.user.index', compact('categories'));
    }

    public function show(int $id)
    {
        $user = $this->userServices->getById($id);
        return response()->json([
            'data' => $user,
            'message' => 'Get user success.'
        ], Response::HTTP_OK);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->userServices->update($request, $id);
        return response()->json([
            'data' => $user,
            'message' => 'Update user success.'
        ], Response::HTTP_OK);
    }

    public function store(StoreUserRequest $request)
    {
        $user = $this->userServices->create($request);
        return response()->json([
            'data' => $user,
            'message' => 'Create user success.'
        ], Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $this->userServices->delete($id);
        return response()->json([
            'data' => true,
            'message' => 'Delete user success.'
        ], Response::HTTP_OK);
    }

    public function getListUser(Request $request)
    {
        $users = $this->userServices->search($request);
        return view('backend.user.list', compact('users'));
    }

    public function getLatestNotification()
    {
        return $this->userServices->getLatestNotification();
    }
}
