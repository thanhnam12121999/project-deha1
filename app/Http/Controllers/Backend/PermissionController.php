<?php

namespace App\Http\Controllers\Backend;

use App\Services\Permission\PermissionService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Permission\StorePermissionRequest;

class PermissionController extends Controller
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }
    public function index()
    {
        $permissions = $this->permissionService->getAll();
        return view('backend.permission.index', compact('permissions'));
    }

    public function store(StorePermissionRequest $request)
    {
        $this->permissionService->create($request);
        return redirect()->back()->with('message', 'Create permission success');   
    }
}
