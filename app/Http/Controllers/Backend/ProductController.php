<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Image\MultipleImageRequest;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Image;
use App\Services\Category\CategoryService;
use App\Services\Image\ImageService;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $productService;
    protected $categoryService;
    protected $imageService;
    /**
     * Call ProductRepository by ProductRepositoryInterface 
     */
    public function __construct(
        ProductService $productService,
        CategoryService $categoryService,
        ImageService $imageService
    ) {
        $this->productService = $productService;
        $this->categoryService  = $categoryService;
        $this->imageService  = $imageService;
    }

    public function index()
    {
        $categories = $this->categoryService->getAll();
        return view('backend.product.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $objectProduct = $this->productService->store($request);
        $product = $this->imageService->saveMutilpleImageFile($request, $objectProduct);
        return response()->json([
            'data' => $product,
            'message' => 'Create Product success !',
        ], Response::HTTP_OK);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $product = new ProductResource($this->productService->getById($id));
        return response()->json([
            'data' => $product,
            'message' => 'Update Product success.'
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, int $id)
    {
        $product = $this->productService->update($request, $id);
        return response()->json([
            'data' => $product,
            'message' => 'Update Product success.'
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $this->productService->delete($id);
        return response()->json([
            'data' => true,
            'message' => 'Delete Product success.'
        ], Response::HTTP_OK);
    }

    public function getlistProduct(Request $request)
    {
        $products = $this->productService->search($request);
        return view('backend.product.list', compact('products'));
    }

    public function updateImages(MultipleImageRequest $request, int $id)//
    {
        $objectProduct = new ProductResource($this->productService->getById($id));
        $product = $this->imageService->saveMutilpleImageFile($request, $objectProduct);
        return response()->json([
            'data' => $product,
            'message' => 'Update Images Product  success.'
        ], Response::HTTP_OK);
    }

    // public function deleteImage(Image $image)
    // {
    //     // $this->imageService->deleteImages($image);
    //     return response()->json([
    //         'data' => true,
    //         'message' => 'Delete Image success.'
    //     ], Response::HTTP_OK);
    // }
}
