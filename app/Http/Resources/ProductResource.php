<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'namecate' => new CategoryResource($this->Category),
            'description' => $this->description,
            'price' => $this->price,
            'status' => $this->status,
            'images' => $this->images
        ];
    }
}
