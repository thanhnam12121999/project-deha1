<?php

namespace App\Http\View\Composers;

use App\Services\User\UserService;
use Illuminate\View\View;

class UserComposer
{
    /**
     * The user repository implementation.
     *
     * @var \App\Repositories\UserRepository
     */
    protected $userService;

    /**
     * Create a new profile composer.
     *
     * @param  \App\Repositories\UserRepository  $users
     * @return void
     */
    public function __construct(UserService $userService)
    {
        // Dependencies are automatically resolved by the service container...
        $this->userService = $userService;
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('userLatestNotification', $this->userService->getLatestNotification());
    }
}