<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\VietnamPhoneRule;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:100',
            'username'  => 'required|min:3|max:12|unique:users',
            'password' => 'max:100',
            'phone' => ['required', new VietnamPhoneRule(), Rule::unique('users')->ignore($this->id)]
        ];
    }
}
