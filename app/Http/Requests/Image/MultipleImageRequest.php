<?php

namespace App\Http\Requests\Image;

use Illuminate\Foundation\Http\FormRequest;

class MultipleImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'multiple-image.*' => 'image|mimes:jpeg,png,jpg,svg|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'multiple-image.*.image' => 'Must be image  .',
            'multiple-image.*.mimes' => 'Only jpeg,png,jpg,svg images are allowed .',
            'multiple-image.*.max' => 'Sorry! Maximum allowed size for an image is 2MB .',
        ];

    }
}
