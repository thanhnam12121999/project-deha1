<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof  ModelNotFoundException) {
            return response()->json([
                "statusCode" => Response::HTTP_NOT_FOUND,
                "message" => $exception->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }
        // 
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        if ($exception instanceof  QueryException) {
            return response()->json([
                "statusCode" => $statusCode,
                "message" => $exception->getMessage()
            ], $statusCode);
        }
        // 
        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json([
                "statusCode" => Response::HTTP_BAD_REQUEST,
                'message' => 'Method is not allowed for the requested route',
            ], Response::HTTP_BAD_REQUEST);
        }
        return parent::render($request, $exception);
    }
}
