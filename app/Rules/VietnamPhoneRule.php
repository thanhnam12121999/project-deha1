<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VietnamPhoneRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return preg_match('/^(\+84|0)'.'(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])'.'[0-9]{7}$/', $value);
    }


    public function validate($attribute, $value): bool
    {
        return $this->passes($attribute, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Phone number is incorrect';
    }
}