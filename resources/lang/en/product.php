<?php

return [
    'tittle-header' => 'List Product',
    'image' => 'Image',
    'name-product' => 'Name Product',
    'quantity' => 'quantity stock',
    'type-product' => 'Type Product',
    'status' => 'Status',
    'view-product' => 'View',
    'update-product' => 'Update',
    'delete-product' => 'Delete',
    'action' => 'Action',
    'add-product' => 'Add New',
    'search' => 'Search',
    'description' => 'Description',
    'price' => 'Price',
    'image-current' => 'Image Current',
    'new-image' => 'New image',
    'mutilple-file' => 'Mutilple File'
];