<?php

return [
    'tittle-header' => 'Danh sách sản phẩm',
    'image' => 'Hình ảnh',
    'name-product' => 'Tên sản phẩm',
    'quantity' => 'Số lượng kho',
    'type-product' => 'Loại sản phẩm',
    'status' => 'Trạng thái',
    'view-product' => 'Xem',
    'update-product' => 'Cập nhật',
    'delete-product' => 'Xóa',
    'action' => 'Hoạt động',
    'add-product' => 'Thêm mới',
    'search' => 'Tìm kiếm',
    'description' => 'Mô tả',
    'price' => 'Giá',
    'image-current' => 'Ảnh hiện tại',
    'new-image' => 'Ảnh mới',
    'mutilple-file' => "Nhiều File"
];