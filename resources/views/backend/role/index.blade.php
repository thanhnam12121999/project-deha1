@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="tittle-header"><i class="glyphicon glyphicon-cd"></i> List Role</h1>
        <div class="breadcrumb">
            <form id="form-role-search" class="app-search">
                @csrf
                <div class="app-search-box">
                    <div class="input-group">
                        <input type="text" id="search-role" class="form-control" data-url="{{ route('roles.list') }}"  placeholder="Search..." name="search">
                        <div class="input-group-append">
                            <div class="btn btn-search" type="submit">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @can('role-add')
            <div class="btn-add">
                <a id="btn-add-role" class="btn btn-primary btn-sm" role="button">
                    <span class="glyphicon glyphicon-plus"></span> Add New
                </a>
            </div>
            @endcan
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box" id="view">
                    <div class="box-header with-border">
                        <div class="box-body">
                            <div class="row row-table" id="data-role" data-url = "{{ route('roles.list') }}">
                                <div id="list-role" style="width: 100%"></div>
                                <!-- /.ND -->
                            </div>
                        </div>
                    </div><!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
    </section>
</div>
@include('backend.role.edit')
@include('backend.role.add')
<script src="{{ asset('assets/js/setup.js') }}"></script>
<script src="{{ asset('assets/js/admin/role.js') }}"></script>

@endsection