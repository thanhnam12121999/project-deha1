<div id="modal-role-edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-style">
        <div class="modal-content">
            <div class="modal-header modal-header-style">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body-main">
                <div class="modal-body data-table">
                    <form id="edit-form-role" method="POST">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="_token" id="token2" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control name-role" id="name-role-edit" name="name">

                                <span class="errors name" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>Display Name</label>
                                <textarea class="form-control display-name" name="display_name" id="display_name"
                                    rows="3"></textarea>


                                <span class="errors display-name" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="row">
                                <div class="col col-md-12">
                                    <label>
                                        <input type="checkbox" class="checked">
                                    </label> Check All
                                </div>
                            </div>
                            @foreach ($permissions as $permission)
                            <div class="card mb-3" style="width: 100%;">
                                <div class="card-header card-header-sty" style="">
                                    <label class="lable-checkbox">
                                        <input type="checkbox" value="" class="checkbox_wrapper">
                                    </label>
                                    Module {{ $permission->name }}
                                </div>
                                <div class="card text-white  mb-3" style="margin: 0!important">
                                    <div class="row">
                                        @foreach ($permission->permissionsChildrent as $permissionsChildrentItem)
                                        <div class="col col-md-3">
                                            <div class="card-body">
                                                <h5 style="font-family: 'Times New Roman', Times, serif"
                                                    class="card-title">
                                                    <label class="lable-checkbox">
                                                        <input type="checkbox"
                                                            id="permission{{$permissionsChildrentItem->id}}"
                                                            name="permission_id[]"
                                                            value="{{$permissionsChildrentItem->id}}"
                                                            class="checkbox_childrent">
                                                    </label>
                                                    {{$permissionsChildrentItem->name}}
                                                </h5>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                </div>
            </div>
            <div class="modal-footer" id="#test13">
                <button id="update-role1" class="btn btn-primary ">Save changes</button>
                <button class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>