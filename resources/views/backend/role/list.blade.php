<div id="table-responsive1" class="table-responsive">
    <table id="data1" class="table table-hover table-bordered table-content">
        <thead>
            <tr>
                <th class="text-center" style="width:20px">ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Display Name</th>
                <th class="text-center" colspan="2">Thao tác</th>
            </tr>
        </thead>
        <tbody>
            <form id="fixx" action="">
                @foreach ($roles as $role)
                <tr>
                    <td class="text-center">{{ $role->id }}</td>
                    <td style="font-size: 16px;">{{ $role->name }}</td>
                    <td style="font-size: 16px;">{{ $role->display_name }}</td>
                    <td class="text-center">
                        @can('role-edit')
                        <button type="button"
                            data-action="{{ route('roles.update', $role->id) }}"
                            data-url-show="{{ route('roles.show', $role->id) }}"
                            data-url="{{ route('roles.update', $role->id) }}"
                            class="btn btn-success btn-xs btn-edit-role"><i
                                class="fa fa-plus" aria-hidden="true"></i> Cập
                            nhật</button>
                        @endcan
                    </td>
                    <td class="text-center">
                        @can('role-delete')
                        <button type="button"
                            data-url="{{ route('roles.delete', $role->id) }}"
                            class="btn btn-danger btn-xs btn-delete-role"><i
                                class="fa fa-trash" aria-hidden="true"></i>
                            Xóa</button>
                        @endcan
                    </td>
                </tr>
                @endforeach
            </form>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {!! $roles->links() !!}
    </div>
</div>