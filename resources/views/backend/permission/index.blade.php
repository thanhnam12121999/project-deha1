@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="tittle-header"><i class="glyphicon glyphicon-cd"></i> Add Permission</h1>
        <div class="breadcrumb" style="height: 40px">
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box" id="view">
                    <div class="box-header with-border">
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if(session()->has('message'))
                            <div class="row">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <strong>Success !</strong>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                            @endif
                            @if ($errors->any())
                            <div class="row">
                                <div id="" class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <strong>Danger !</strong>
                                    @foreach ($errors->all() as $error)
                                    <span>{{ $error }}. &nbsp;</span>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            <div class="row" style='padding:0px; margin:0px;'>
                                <!--ND-->
                                <div class="col col-md-8">
                                    {{-- @can('permission-add') --}}
                                    <form action="{{ route('permissions.store') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label>Permissions parent</label>
                                            <select name="group" class="form-control"
                                                id="parent_permission">
                                                <option value="">Choose Module</option>
                                                @foreach (config('permission.permission_parents') as
                                                $permission_parents)
                                                <option value="{{ $permission_parents }}">{{ $permission_parents }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-check mb-3">
                                            <div class="row">
                                                @foreach (config('permission.permission_childrent') as
                                                $moduleItemChildrent)
                                                <div class="col col-md-3 mb-3">
                                                    <input class="form-check-input" name="childs[]"
                                                        type="checkbox" value="{{ $moduleItemChildrent }}">
                                                    <label class="form-check-label">
                                                        {{ $moduleItemChildrent }}
                                                    </label>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                    {{-- @endcan --}}
                                </div>
                                <!-- /.ND -->
                            </div>
                        </div>
                    </div><!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection