@foreach ($categories as $category)
<option value="{{ $category->id }}">{{ $category->name }}</option>

@if (count($category->childs) > 0)
@include('backend.category.subcategories', ['subcategories' => $category->childs,
'parent' => $category->name, 'parent_id' => $category->id])
@endif

@endforeach