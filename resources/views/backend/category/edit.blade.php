<div id="modal-edit" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-style">
        <div class="modal-content">
            <div class="modal-header modal-header-style">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body-main">
                <div class="modal-body data-table">
                    <form id="editFormCate" method="POST">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="_token" id="token2" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Name</label><span style="color: red"> (*)</span>
                                <input type="text" class="form-control" id="name-cate" name="name">

                                <span class="errors name" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="form-group url-list-select"
                                url-list-select="{{route('categories.list_select')}}">
                                <label>parents</label>
                                <select name="parent_id" id="category-select" class="form-control category-select">
                                </select>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-update-category" type="submit" class="btn btn-primary">Save changes</button>
                <button class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>