<div id="table-responsive1" class="table-responsive">
    <table id="data1" class="table table-hover table-bordered table-content" style="">
        <thead>
            <tr>
                <th class="text-center">ID</th>
                <th>Name</th>
                <th class="text-center" colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
            <form id="fix" action="">
                @foreach ($catagories as $category)
                <tr>
                    <td class="text-center">{{ $category->id }}</td>
                    <td class="text-center">{{ $category->name }}</td>
                    <td class="text-center">
                        @can('category-edit')
                        <button type="button"
                            url-update="{{ route('categories.update', $category->id) }}"
                            url-show="{{ route('categories.show', $category->id) }}"
                            data-url="{{ route('categories.update', $category->id) }}"
                            class="btn btn-success btn-xs btn-edit-category"><i
                                class="fa fa-plus" aria-hidden="true"></i> Cập
                            nhật</button>
                        @endcan
                    </td>
                    <td class="text-center">
                        @can('category-delete')
                        <button type="button" id-product-btn=""
                            data-url="{{ route('categories.delete', $category->id) }}"
                            class="btn btn-danger btn-xs  btn-delete"><i
                                class="fa fa-trash" aria-hidden="true"></i>
                            Xóa</button>
                        @endcan
                    </td>
                </tr>
                @endforeach
            </form>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {!! $catagories->links() !!}
    </div>
</div>