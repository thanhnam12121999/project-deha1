@foreach ($subcategories as $subcategory)
<option value="{{ $subcategory->id }}">{{ $parent}} -> {{ $subcategory->name }}</option>

@if (count($subcategory->childs) > 0)
@php
$parents = $parent . '->' . $subcategory->name;
@endphp
@include('backend.category.subcategories', ['subcategories' => $subcategory->childs, 'parent' => $parents])
@endif

@endforeach