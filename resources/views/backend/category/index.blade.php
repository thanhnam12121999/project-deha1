@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="tittle-header"><i class="glyphicon glyphicon-cd"></i> List Category</h1>
        <div class="breadcrumb">
            <form id="form-category-search" class="app-search">
                @csrf
                <div class="app-search-box">
                    <div class="input-group">
                        <input type="text" id="search-category" class="form-control"
                            data-url="{{ route('categories.list') }}" placeholder="Search..." name="search">
                        <div class="input-group-append">
                            <div class="btn btn-search" type="submit">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @can('category-add')
            <div class="btn-add">
                <a class="btn btn-primary btn-sm btn-show-add-modal" role="button">
                    <span class="glyphicon glyphicon-plus"></span> Add New
                </a>
            </div>
            @endcan
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box" id="view">
                    <div class="box-header with-border">
                        <div class="box-body">
                            @if ($errors->any())
                            <div>
                                <div id="" class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <strong>Danger !</strong>
                                    @foreach ($errors->all() as $error)
                                    <span>{{ $error }}. &nbsp;</span>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            <div class="row row-table" id="data-category" data-url="{{route('categories.list')}}">
                                <div id="list-category" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
@include('backend.category.add')
@include('backend.category.edit')
<script src="{{ asset('assets/js/admin/category.js') }}"></script>

@endsection