<div id="modal-user-add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-style">
        <div class="modal-content">
            <div class="modal-header modal-header-style">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body-main">
                <div class="modal-body data-table">
                    <form id="add-form-user" method="POST">
                        @csrf
                        <input type="hidden" name="_token" id="token2" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control name-users" id="name-user" name="name">

                                <span class="error-name" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>UserName</label>
                                <input type="text" class="form-control error-usernames" id="username" name="username">

                                <span class="errors username" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control phone-edit" id="phone-edit1" name="phone">

                                <span class="errors phone" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="form-group">
                                <label>Select Role</label>
                                <select class="form-control slect2_init" name="role_id[]" multiple>
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-url="{{ route('users.store') }}" id="btn-save-user" type="submit"
                    class="btn btn-primary">Save changes</button>
                <button class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>