<div id="table-responsive1" class="table-responsive">
    <table id="data1" class="table table-hover table-bordered table-content">
        <thead>
            <tr>
                <th class="text-center" style="width:20px">ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Username</th>
                <th class="text-center" colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
            <form id="fixx" action="">
                @foreach ($users as $user)
                <tr>
                    <td class="text-center">{{ $user->id }}</td>
                    <td style="font-size: 16px;">{{ $user->name }}</td>
                    <td style="font-size: 16px;">{{ $user->username }}</td>
                    <td class="text-center">
                        @can('user-edit')
                        <button type="button"
                            data-action="{{ route('users.update', $user->id) }}"
                            data-url-show="{{ route('users.show', $user->id) }}"
                            data-url="{{ route('users.update', $user->id) }}"
                            class="btn btn-success btn-xs btn-update-user"><i
                                class="fa fa-plus" aria-hidden="true"></i> Cập
                            nhật</button>
                        @endcan
                    </td>
                    <td class="text-center">
                        @can('user-delete')
                        <button type="button"
                            data-url="{{ route('users.destroy', $user->id) }}"
                            class="btn btn-danger btn-xs btn-delete-user"><i
                                class="fa fa-trash" aria-hidden="true"></i>
                            Xóa</button>
                        @endcan
                    </td>
                </tr>
                @endforeach
            </form>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {!! $users->links() !!}
    </div>
</div>