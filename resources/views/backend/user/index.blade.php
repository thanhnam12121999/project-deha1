@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="tittle-header"><i class="glyphicon glyphicon-cd"></i> List User</h1>
        <div class="breadcrumb">
            <form id="form-user-search" class="app-search">
                @csrf
                <div class="app-search-box">
                    <div class="input-group">
                        <input type="text" id="search-user" class="form-control" data-url="{{ route('users.list') }}"  placeholder="Search..." name="search">
                        <div class="input-group-append">
                            <div class="btn btn-search" type="submit">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @can('user-add')
            <div class="btn-add">
                <a id="btn-add-user" class="btn btn-primary btn-sm" role="button">
                    <span class="glyphicon glyphicon-plus"></span> Add New
                </a>
            </div>
            @endcan
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box" id="view">
                    <div class="box-header with-border">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row row-table" id="data-user" data-url="{{route('users.list')}}">
                                <!--ND-->
                                <div id="list-user" style="width: 100%"></div>
                                <!-- /.ND -->
                            </div>
                        </div><!-- ./box-body -->
                    </div><!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@include('backend.user.add')
@include('backend.user.edit')
<script src="{{ asset('assets/js/setup.js') }}"></script>
<script src="{{ asset('assets/js/admin/user.js') }}"></script>

@endsection