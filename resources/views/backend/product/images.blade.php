<div id="modal-images" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-style">
        <div class="modal-content">
            <div class="modal-header modal-header-style">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body-main">
                <div class="modal-body">
                    <form id="images-form" method="POST" enctype="multipart/form-data">
                        @method('POST')
                        @csrf
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="row mx-2">
                                <div class="form-group">
                                    <label class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <input class="file-upload" type="file" name="multiple-image[]" multiple
                                            style="display: none">
                                        <span>Add File</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success update-images mx-3" style="height: 35px">
                                        <i class="fa fa-upload" aria-hidden="true"></i>
                                        <span>Upload</span>
                                    </button>

                                    <p class="errors multiple-image" style="color: red" role="alert">
                                        <strong></strong>
                                    </p>
                                </div>
                                <div class="form-group">

                                </div>
                            </div>

                            <div id='images-render'></div>
                            {{-- --}}
                            <script id="download-template" type="text/x-handlebars-template">
                                <table role="presentation" class="table table-striped">
                                    <tbody class="files">
                                        @{{#each images}} 
                                        <tr>
                                            <td style="width: 20%">
                                                <span class="preview">
                                                    <a href="">
                                                        <img src="assets/images/products/@{{ file }}" alt="" style="height: 60px;">
                                                    </a>
                                                </span>
                                            </td>
                                            <td class="form-group" style="width: 60%">
                                                <input class="form-control" name="multiple-image[]" value="@{{ file }}" 
                                                    style="background-color: #b1c4de;border: none" disabled>
                                                <input type="hidden" class="delete-image" name="delete-image[]" multiple disabled>
                                            </td>
                                            <td>
                                            <button class="btn btn-danger btn-delete-image-current" 
                                                data-url="{{url()->current()}}/images/@{{ id }}" 
                                                data-url-show="{{url()->current()}}/@{{ imageable_id }}"
                                                data-type="DELETE" data-id='@{{id}}' data-name='@{{ file }}'> 
                                                <i class="fa fa-times" aria-hidden="true"></i>                                   
                                            </button> 
                                            </td>
                                        </tr>
                                        @{{/each}}
                                    </tbody>
                                </table>
                            </script>
                            {{-- --}}
                            <script id="upload-template" type="text/x-handlebars-template">
                                <table role="presentation" class="table table-striped">
                                    <tbody class="files">
                                        <tr>
                                            <td style="width: 20%">
                                                <span class="preview">
                                                    <img class="" src="@{{images}}" alt="" style="height: 60px;">      
                                                </span>
                                            </td>
                                            <td style="width: 60%">
                                                <p class="name">@{{nameImage}}</p>
                                                <strong class="error text-danger"></strong>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-delete-image-new" name-image="@{{nameImage}}"> 
                                                    <i class="fa fa-times" aria-hidden="true"></i>                                   
                                                </button> 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                             </script>
                            {{-- --}}
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>