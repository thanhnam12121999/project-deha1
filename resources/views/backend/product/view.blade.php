<div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-style">
        <div class="modal-content">
            <div class="modal-header modal-header-style">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body1">
                <div class="row modal-row" style="
                margin-left: 0px;
            ">
                    <div class="col col-md 6">
                        <img class="img-modal-row" style="" src="" alt="">
                    </div>
                    <div class="col col-md 6">
                        <h1 id="modal-name" class="pt-3 my-5 name-product-modal " style="
                        color: #d63131;">
                        </h1>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span><br><br>
                        <span style="font-size: larger">{{ trans('product.price') }} :</span>
                        <span id="price-view" style="color: red"></span><br><br>
                        <span style="font-size: larger;">{{ trans('product.type-product') }} : </span>
                        <span class="name-category-modal" id="modal-category"></span><br>
                        <p style="font-size: larger">{{ trans('product.description') }} :</p>
                        <p id="description"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>