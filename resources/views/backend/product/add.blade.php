<div id="modal-add" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-style">
        <div class="modal-content">
            <div class="modal-header modal-header-style">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body-main">
                <div class="modal-body data-table">
                    <form data-url="{{ route('products.store') }}" method="POST" id="add-form-product"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_token" id="token1" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>{{ trans('product.name-product') }}</label><span style="color: red"> (*)</span>
                                <input type="text" class="form-control name-products" id="name-product-add" name="name">

                                <span class="errors name" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('product.type-product') }}</label>
                                <select name="category_id" id="cate-id-add" class="form-control">
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="Description">{{ trans('product.description') }}</label>
                                <textarea style="overflow-y: scroll" class="form-control" name="description" rows="5"
                                    id="description-add">{{ trans('product.description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('product.price') }}</label><span style="color: red"> (*)</span>
                                <input type="text" class="form-control price-product" name="price" id="price">

                                <span class="errors price" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>

                        <div class="form-group">
                            <label>Multiple Image</label>
                            <input multiple="multiple" type="file" class="form-control image-input" name="multiple-image[]">
                        </div>

                        <div class="form-group">
                            <label>{{ trans('product.status') }}</label>
                            <select name="status" id="status-add" class="form-control">
                                <option id="status-add0" value="0">Inactive</option>
                                <option selected id="status-add1" value="1">Active</option>
                            </select>
                        </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button id="btn-add-product" type="button" class="btn btn-primary">Save changes</button>
            <button class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>