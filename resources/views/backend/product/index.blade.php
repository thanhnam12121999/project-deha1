@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="tittle-header"><i class="glyphicon glyphicon-cd"></i>{{ trans('product.tittle-header') }}</h1>
        <div class="breadcrumb">
            <form id="form-product-search" data-url="{{route('products.list')}}" class="app-search">
                @csrf
                <div class="app-search-box">
                    <div class="input-group">
                        <select name="category" id="search-category" data-url="{{route('products.list')}}"
                            class="form-control mr-3">
                            <option value="">Search Category</option>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>

                        <input type="text" id="search-product" class="form-control"
                            data-url="{{ route('products.list') }}" placeholder="{{ trans('product.search') }}..."
                            name="search">
                        <div class="input-group-append">
                            <div class="btn btn-search" type="submit">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @can('product-add')
            <div class="btn-add">
                <a id="btn-form-add" class="btn btn-primary btn-sm" role="button">
                    <span class="glyphicon glyphicon-plus"></span>{{ trans('product.add-product') }}
                </a>
            </div>
            @endcan
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box" id="view">
                    <div class="box-header with-border">
                        <div class="box-body">
                            @if ($errors->any())
                            <div>
                                <div id="" class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <strong>Danger !</strong>
                                    @foreach ($errors->all() as $error)
                                    <span>{{ $error }}. &nbsp;</span>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            <div class="row row-table" id="data-product" data-url="{{ route('products.list') }}">
                                <div id="list-product" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
@include('backend.product.view')
@include('backend.product.edit')
@include('backend.product.add')
@include('backend.product.images')
<script src="{{ asset('assets/js/admin/product.js') }}"></script>

@endsection