<div id="modal-edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-style">
        <div class="modal-content">
            <div class="modal-header modal-header-style">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body-main">
                <div class="modal-body data-table">
                    <form id="editFormProduct" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="_token" id="token2" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>{{ trans('product.name-product') }}</label><span style="color: red"> (*)</span>
                                <input type="text" class="form-control name-products" id="name-product" name="name">

                                <span class="errors name" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('product.type-product') }}</label>
                                <select name="category_id" id="cate-id" class="form-control">
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description"
                                    style="overflow-y: scroll">{{ trans('product.description') }}</label>
                                <textarea class="form-control" name="description" rows="5"
                                    id="description1">{{ trans('product.description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('product.status') }}</label>
                                <select name="status" id="status" class="form-control">
                                    <option id="status0" value="0">0</option>
                                    <option id="status1" selected value="1">1</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('product.price') }} (VND)</label><span style="color: red"> (*)</span>
                                <input type="text" class="form-control price-product" name="price" id="price">

                                <span class="errors price" style="color: red" role="alert">
                                    <strong></strong>
                                </span>
                            </div>

                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary update-product">Save changes</button>
                <button class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>