<div id="render-list-product">
    <div id="table-responsive1" class="table-responsive">
        <table id="data1" class="table table-hover table-bordered table-content">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">{{ trans('product.image') }}</th>
                    <th class="text-center">{{ trans('product.name-product') }}</th>
                    <th class="text-center">{{ trans('product.quantity') }}</th>
                    <th class="text-center">{{ trans('product.type-product') }}</th>
                    <th class="text-center">{{ trans('product.status') }}</th>
                    <th class="text-center">{{ trans('product.view-product') }}</th>
                    <th class="text-center" colspan="3">{{ trans('product.action') }}</th>
                </tr>
            </thead>
            <tbody>
                <form id="fixx" action="">
                    @foreach ($products as $product)
                    <tr id="id-product{{ $product->id }}">
                        <td class="text-center">{{ $product->id }}</td>
                        <td style="width:70px">
                            @if (!empty($product->firstImage))
                            <img class="img-table-col" src="assets/images/products/{{ $product->firstImage->file }}"
                                alt="{{ $product->name }}" class="img-responsive">
                            @else
                            <img class="img-table-col" src="assets/images/products/default.jpg"
                                alt="{{ $product->name }}" class="img-responsive">
                            @endif
                        </td>
                        <td style="font-size: 16px;">{{ $product->name }}</td>
                        <td class="text-center">{{ $product->id }}</td>
                        <td class="text-center">{{ $product->category->name }}</td>
                        <td class="text-center">
                            @if ($product->status == 1)
                            <i style="color: green" class="fa fa-check" aria-hidden="true"></i>
                            @else
                            <i style="color: red" class="fa fa-times" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="text-center">
                            @can('product-show')
                            <button type="button" data-url="{{ route('products.show', $product->id) }}"
                                class="btn btn-info btn-xs btnview"><i class="fa fa-eye" aria-hidden="true"></i>
                                {{ trans('product.view-product') }}</button>
                            @endcan
                        </td>
                        <td class="text-center">
                            @can('product-edit')
                            <button type="button" url-update="{{ route('products.update', $product->id) }}"
                                data-url-show="{{ route('products.show', $product->id) }}"
                                data-url="{{ route('products.update', $product->id) }}"
                                class="btn btn-success btn-xs btn-edit-product"><i class="fa fa-plus"
                                    aria-hidden="true"></i>{{ trans('product.update-product') }}</button>
                            @endcan
                        </td>
                        <td class="text-center">
                            @can('product-upload')
                            <button type="button" url-update="{{ route('products.update_image', $product->id) }}"
                                data-url-show="{{ route('products.show', $product->id) }}"
                                class="btn btn-success btn-xs btn-edit-images"><i class="fa fa-upload"
                                    aria-hidden="true"></i>Images</button>
                            @endcan
                        </td>
                        <td class="text-center">
                            @can('product-delete')
                            <button type="button" id-product-btn="{{ $product->id }}"
                                data-url="{{ route('products.destroy', $product->id) }}"
                                class="btn btn-danger btn-xs  btn-delete"><i class="fa fa-trash" aria-hidden="true"></i>
                                {{ trans('product.delete-product') }}</button>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </form>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {!! $products->links() !!}
        </div>
    </div>
</div>

