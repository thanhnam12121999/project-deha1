
<div class="left-side-menu">

    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Navigation</li>
                <li>
                    <a  href="{{ route('categories.index') }}" class="waves-effect">
                        <i class="ion-md-basket"></i>
                        <span> Category </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('products.index') }}" class="waves-effect">
                        <i class="ion-md-speedometer"></i>
                        <span>  Product  </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}" class="waves-effect">
                        <i class="ion-md-basket"></i>
                        <span>  User </span>
                    </a>
                </li>
                <li>
                    <a  href="{{ route('roles.index') }}" class="waves-effect">
                        <i class="ion-md-basket"></i>
                        <span> Roles </span>
                    </a>
                </li>
                <li>
                    <a  href="{{ route('permissions.index') }}" class="waves-effect">
                        <i class="ion-md-basket"></i>
                        <span> Permissions </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>

