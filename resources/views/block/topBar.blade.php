
<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">

        <li class="dropdown d-none d-lg-block">
            @php
            $language = Session::get('website_language', config('app.locale'));
            @endphp
            {{-- language current --}}
            <a href="{!! route('change_language', [$language]) !!}" class="nav-link dropdown-toggle mr-0 waves-effect"
                data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="assets\images\flags\{{$language}}.jpg" alt="user-image" class="mr-2" height="12">
                <span class="align-middle">{{ $language }}
                    <i class="mdi mdi-chevron-down"></i></span>
            </a>
            {{--list language--}}
            <div class="dropdown-menu dropdown-menu-right">
                @foreach (File::glob('assets/images/flags/*') as $file)
                <a href="{!! route('change_language', [pathinfo($file, PATHINFO_FILENAME)]) !!}"
                    class="dropdown-item notify-item">
                    <img src="{{ $file }}" alt="user-image" class="mr-2" height="12"> <span
                        class="align-middle">{{ pathinfo($file, PATHINFO_FILENAME) }}</span>
                </a>
                @endforeach
            </div>
        </li>

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle  waves-effect" data-toggle="dropdown" href="#" role="button"
                aria-haspopup="false" aria-expanded="false">
                <i class="mdi mdi-email-outline noti-icon"></i>
                <span class="badge badge-purple rounded-circle noti-icon-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="font-16 m-0">
                        <span class="float-right">
                            <a href="" class="text-dark">
                                <small>Clear All</small>
                            </a>
                        </span>Chat
                    </h5>
                </div>

                <div class="slimscroll noti-scroll">

                    <div class="inbox-widget">
                        <a href="#">
                            <div class="inbox-item">
                                <div class="inbox-item-img"><img src="assets\images\users\avatar-1.jpg"
                                        class="rounded-circle" alt=""></div>
                                <p class="inbox-item-author">Cristina Pride</p>
                                <p class="inbox-item-text text-truncate">Hi, How are you? What about our next meeting
                                </p>
                            </div>
                        </a>
                        <a href="#">
                            <div class="inbox-item">
                                <div class="inbox-item-img"><img src="assets\images\users\avatar-2.jpg"
                                        class="rounded-circle" alt=""></div>
                                <p class="inbox-item-author">Sam Garret</p>
                                <p class="inbox-item-text text-truncate">Yeah everything is fine</p>
                            </div>
                        </a>
                        <a href="#">
                            <div class="inbox-item">
                                <div class="inbox-item-img"><img src="assets\images\users\avatar-3.jpg"
                                        class="rounded-circle" alt=""></div>
                                <p class="inbox-item-author">Karen Robinson</p>
                                <p class="inbox-item-text text-truncate">Wow that's great</p>
                            </div>
                        </a>
                        <a href="#">
                            <div class="inbox-item">
                                <div class="inbox-item-img"><img src="assets\images\users\avatar-4.jpg"
                                        class="rounded-circle" alt=""></div>
                                <p class="inbox-item-author">Sherry Marshall</p>
                                <p class="inbox-item-text text-truncate">Hi, How are you? What about our next meeting
                                </p>
                            </div>
                        </a>
                        <a href="#">
                            <div class="inbox-item">
                                <div class="inbox-item-img"><img src="assets\images\users\avatar-5.jpg"
                                        class="rounded-circle" alt=""></div>
                                <p class="inbox-item-author">Shawn Millard</p>
                                <p class="inbox-item-text text-truncate">Yeah everything is fine</p>

                            </div>
                        </a>
                    </div> <!-- end inbox-widget -->

                </div>
                <!-- All-->
                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                    View all
                    <i class="fi-arrow-right"></i>
                </a>

            </div>
        </li>

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle  waves-effect" data-toggle="dropdown" role="button"
                aria-haspopup="false" aria-expanded="false">
                <i class="mdi mdi-bell-outline noti-icon"></i>
                <span class="badge badge-pink rounded-circle noti-icon-badge">4</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="font-16 m-0">
                        <span class="float-right">
                            <a href="" class="text-dark">
                                <small>Clear All</small>
                            </a>
                        </span>Notification
                    </h5>
                </div>

                <div class="slimscroll noti-scroll infinite-scroll">
                    <div class="content-load">
                        @foreach ($userLatestNotification as $notification)
                        <!-- item-->
                        <a href="" class="dropdown-item notify-item">
                            <div class="notify-icon text-success">
                                <i class="mdi mdi-account-plus text-primary"></i>
                            </div>
                            <p class="notify-details">{{ $notification->data['content'] }}
                                <small class="noti-time">{{ $notification->created_at->diffForHumans() }}</small>
                            </p>
                        </a>
                        @endforeach
                        {{ $userLatestNotification->links() }}
                    </div>
                </div>

                <!-- All-->
                <a href="" class="dropdown-item text-center notify-item notify-all">
                    See all notifications
                </a>

            </div>
        </li>

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button"
                aria-haspopup="false" aria-expanded="false">
                <img src="assets\images\users\avatar-1.jpg" alt="user-image" class="rounded-circle">
                <span class="pro-user-name ml-1">
                    Thompson <i class="mdi mdi-chevron-down"></i>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome !</h6>
                </div>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="mdi mdi-account-outline"></i>
                    <span>Profile</span>
                </a>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="mdi mdi-settings-outline"></i>
                    <span>Settings</span>
                </a>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="mdi mdi-lock-outline"></i>
                    <span>Lock Screen</span>
                </a>

                <div class="dropdown-divider"></div>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="mdi mdi-logout-variant"></i>
                    <span>Logout</span>
                </a>

            </div>
        </li>

        <li class="dropdown notification-list">
            <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect">
                <i class="mdi mdi-settings-outline noti-icon"></i>
            </a>
        </li>


    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="index.html" class="logo text-center logo-dark">
            <span class="logo-lg">
                <img src="assets\images\logo-dark.png" alt="" height="18">
                <!-- <span class="logo-lg-text-dark">Velonic</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-lg-text-dark">V</span> -->
                <img src="assets\images\logo-sm.png" alt="" height="22">
            </span>
        </a>

        <a href="index.html" class="logo text-center logo-light">
            <span class="logo-lg">
                <img src="assets\images\logo-light.png" alt="" height="18">
                <!-- <span class="logo-lg-text-dark">Velonic</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-lg-text-dark">V</span> -->
                <img src="assets\images\logo-sm.png" alt="" height="22">
            </span>
        </a>
    </div>

    <!-- LOGO -->


    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile waves-effect">
                <i class="mdi mdi-menu"></i>
            </button>
        </li>
    </ul>
</div>

<script src="{{ asset('assets/js/admin/header.js') }}"></script>