<!DOCTYPE html>
<html lang="en">

<head>
    <base href="http://localhost/">
    <meta charset="utf-8">
    <title>Dashboard 1 | Velonic - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive bootstrap 4 admin template" name="description">
    <meta content="Coderthemes" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Plugins css-->
    <link href="{{ asset('assets\libs\sweetalert2\sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
    <!-- App css -->
    <link rel="stylesheet" href="{{ asset('assets/libs/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets\css\pages\product.css') }}" type="text/css">
    <link href="{{ asset('assets\css\bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet">
    <link href="{{ asset('assets\css\icons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets\css\app.min.css') }}" rel="stylesheet" type="text/css" id="app-stylesheet">
    <link href="{{ asset('assets\css\pages\app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets\css\pages\app-modal.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('assets\js\jquery-3.6.0.min.js') }}"></script>
    <!-- Select 2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body>
    <div id="wrapper">
        <div class="content-page" style="margin-left: 0">
            @yield('content')
        </div>
    </div>
    <script src="{{ asset('assets\js\vendor.min.js') }}"></script>

    <script src="{{ asset('assets\libs\sweetalert2\sweetalert2.min.js') }}"></script>
    <!-- App js -->
    <script src="{{ asset('assets\js\app.min.js') }}"></script>
    <!-- Select2 -->
</body>

</html>