$(document).ready(() => {
    const TIME_SEARCH = 1000;

    async function getListCategory() {
        let url = $('#data-category').attr('data-url');
        return callAjax(url, 'GET')
            .then((data) => {
                $('#list-category').html(data);
            })
    }
    getListCategory()

    async function getListSelectHtml() {
        let url = $('.url-list-select').attr('url-list-select');
        return callAjax(url, 'GET')
            .then((data) => {
                $('.category-select').html(data);
            })
    }
    getListSelectHtml()

    //Update 
    $(document).on('click', '#btn-update-category', function(e) {
        e.preventDefault()
        let url = $(this).attr('data-url')
        let formData = new FormData($('#editFormCate')[0]);
        removeErrorFormModal()
        callAjax(url, 'POST', formData)
            .then(() => {
                getListCategory()
                getListSelectHtml()
            })
            .then(() => {
                $('#modal-edit').modal('hide')
            })
            .catch((err) => {
                fillErrorFormModal(err.responseJSON.errors) //ajax setup
            })
    })

    //Edit
    $(document).on('click', '.btn-edit-category', function(e) {
        e.preventDefault()
        let urlUpdate = $(this).attr('url-update')
        let urlShow = $(this).attr('url-show')
        $('#btn-update-category').attr('data-url', urlUpdate)
        $('#editFormCate').trigger('reset')
        removeErrorFormModal()
        const editCategory = async() => {
            await getListSelectHtml()
            const data = await callAjax(urlShow, "GET")
            fillEditCategory(data)
        }
        editCategory()
            .then(() => {
                $('#modal-edit').modal('show')
            })
    })

    //Edit add
    $(document).on('click', '.btn-show-add-modal', function(e) {
        e.preventDefault()
        $('#add-form-cate').trigger('reset')
        removeErrorFormModal()
        getListSelectHtml()
            .then(() => {
                $('#modal-add').modal('show')
            })
    })

    //Add
    $(document).on('click', '#btn-add-category', function(e) {
        e.preventDefault()
        let url = $('#add-form-cate').attr('data-url')
        var formData = new FormData($('#add-form-cate')[0])
        removeErrorFormModal()
        callAjax(url, "POST", formData)
            .then(() => {
                getListCategory()
                $('#modal-add').modal('hide')
            })
            .catch((err) => {
                fillErrorFormModal(err.responseJSON.errors) //ajax setup
            })
    })

    // Delete
    $(document).on('click', '.btn-delete', function(e) {
        e.preventDefault()
        let url = $(this).attr('data-url')
        let data = { "_token": $('#token').val() }
        const deleteCategory = async() => {
            const result = await swalFireConfirmData()
            if (result.isConfirmed) {
                const dataDelete = await callAjax(url, 'DELETE', data)
                await getListCategory()
                successDelete(dataDelete)
            }
        }
        deleteCategory()
    })

    async function swalFireConfirmData() {
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
        })
    }

    function successDelete(data) {
        Swal.fire({
            title: "Message",
            text: data.message,
            type: "success",
            icon: "success"
        })
    }

    function fillEditCategory(response) {
        $("#category-select option:contains(" + response.data.name + ")").remove()
        $("#category-select option:contains(Select)").remove()
        if (response.data.parent_id == 0) {
            $('#category-select').append('<option selected value="0">Select</option>')
        }
        $('#category-select').val(response.data.parent_id).attr('selected', 'selected')
        $('#name-cate').val(response.data.name)
    }

    //Pagination
    $('body').on('click', '.pagination li a', function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        callAjax(url)
            .then(function(response) {
                $('#list-category').html(response);
            })
    });

    //search
    $(document).on('keyup', '#search-category', debounce(function() {
        let url = $(this).attr('data-url');
        let dataForm = $('#form-category-search').serialize();
        callAjax(url, 'GET', dataForm)
            .then((data) => {
                $('#list-category').html(data);
            })
    }, TIME_SEARCH));

})