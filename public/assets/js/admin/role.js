$(document).ready(() => {

    const TIME_SEARCH = 1000

    async function getListRole() {
        let url = $('#data-role').attr('data-url');
        
        return callAjax(url, 'GET')
            .then((data) => {
                $('#list-role').html(data);
            });
    }
    getListRole();

    // update
    $(document).on('click', 'button#update-role1', function(e) {
        e.preventDefault();
        let url = $(this).attr('data-url');
        let formData = new FormData($('#edit-form-role')[0]);
        
        callAjax(url, 'POST', formData)
            .then(() => {
                getListRole()
                $('#modal-role-edit').modal('hide');
            })
            .catch((err) => {
                fillErrorFormModal(err.responseJSON.errors);
            })
    })

    // Edit
    $(document).on('click', '.btn-edit-role', function(e) {
        e.preventDefault();
        $('#edit-form-role').trigger("reset")
        removeErrorFormModal()
        $('#update-role1').attr('data-url', $(this).attr('data-url'))
        let url = $(this).attr('data-url-show')
        
        callAjax(url, "GET")
            .then((data) => {
                fillEditDataRole(data);
            }).then(() => {
                $('#modal-role-edit').modal('show')
            });
    })

    // Edit Add
    $(document).on('click', '#btn-add-role', function(e) {
        e.preventDefault();
        $('#add-form-role').trigger("reset")
        removeErrorFormModal()
        $('#modal-role-add').modal('show')
    })

    // Add
    $(document).on('click', '#btn-save-role', function(e) {
        e.preventDefault();
        let url = $(this).attr('data-url');
        var formData = new FormData($('#add-form-role')[0]);
        callAjax(url, "POST", formData)
            .then(() => {
                getListRole()
                $('#modal-role-add').modal('hide');
            })
            .catch((err) => {
                fillErrorFormModal(err.responseJSON.errors);
            })
    })

    // Delete
    $(document).on('click', '.btn-delete-role', function(e) {
        e.preventDefault();
        let url = $(this).attr('data-url');
        var data = { "_token": $('#token').val() };
        async function deleteRole() {
            const result = await swalFireConfirmData()
            if (result.isConfirmed) {
                
                const dataDelete = await callAjax(url, 'DELETE', data)
                await getListRole()
                deleteRoleSuccess(dataDelete)
            }
        }
        deleteRole()
    })

    async function swalFireConfirmData() {
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
        })
    }

    function deleteRoleSuccess() {
        Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
        )
    }

    function fillEditDataRole(response) {
        let role = response.data.permissionsChecked;
        $('#name-role-edit').val(response.data.name);
        $('#display_name').val(response.data.display_name);
        for (const key in role) {
            $("#permission" + role[key].id).prop('checked', true);
        }
    }

    //Pagination
    $('body').on('click', '.pagination li a', function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        
        callAjax(url)
            .then(function(response) {
                $('#list-role').html(response);
            })
    });

    let searchRole = debounce(function() {
        let url = $(this).attr('data-url');
        let dataForm = $('#form-role-search').serialize();
        
        callAjax(url, 'GET', dataForm)
            .then((data) => {
                $('#list-role').html(data);
            })
    }, TIME_SEARCH);

    $(document).on('keyup', '#search-role', searchRole);

    // checkbox
    $(function() {
        $('.checkbox_wrapper').on('click', function() {
            $(this).parents('.card').find('.checkbox_childrent').prop('checked', $(this).prop('checked'));
        });
        $('.checked').on('click', function() {
            $(this).parents().find('.checkbox_childrent').prop('checked', $(this).prop('checked'));
            $(this).parents().find('.checkbox_wrapper').prop('checked', $(this).prop('checked'));
        });
    })
});