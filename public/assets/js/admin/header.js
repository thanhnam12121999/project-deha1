$(document).ready(function() {
    $('ul.pagination').hide()
    $(() => {
        let imageLoader = 'assets/images/loader/loading.gif';
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img style="margin-left:auto;margin-right:auto;display:block" src="' + imageLoader + '"/>',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.content-load',
            callback: () => {
                $('ul.pagination').remove();
            }
        });
    });
});