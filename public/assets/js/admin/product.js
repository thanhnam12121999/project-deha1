$(document).ready(() => {
    const URL_IMAGE = "assets/images/products/"; //
    const TIME_SEARCH = 1000;

    async function getListProducts() {
        let url = $("#data-product").attr("data-url");
        showLoader();
        return callAjax(url, "GET")
            .then(data => {
                $("#list-product").html(data);
            })
            .finally(() => {
                hideLoader();
            });
    }
    getListProducts();

    // view
    $(document).on("click", ".btnview", function(e) {
        e.preventDefault(); //
        let url = $(this).attr("data-url");
        showLoader();
        callAjax(url, "GET")
            .then(response => {
                fillDataViewProduct(
                    response.data.product,
                    response.data.images
                ); //relation
            })
            .then(() => {
                $("#view-modal").modal("show");
            })
            .finally(() => {
                hideLoader();
            });
    });

    // update
    $(document).on("click", ".update-product", function(e) {
        e.preventDefault();
        let url = $(this).attr("data-url");
        let formData = new FormData($("#editFormProduct")[0]); //put
        removeErrorFormModal();
        showLoader();
        callAjax(url, "POST", formData)
            .then(() => {
                getListProducts();
                $("#modal-edit").modal("hide");
            })
            .catch(err => {
                fillErrorFormModal(err.responseJSON.errors); //ajax setup
            })
            .finally(() => {
                hideLoader();
            });
    });

    // Edit
    $(document).on("click", ".btn-edit-product", function(e) {
        e.preventDefault();
        removeErrorFormModal();
        let urlUpdate = $(this).attr("url-update");
        $(".update-product").attr("data-url", urlUpdate);
        $("#editFormProduct").trigger("reset");
        let url = $(this).attr("data-url-show");
        showLoader();
        callAjax(url, "GET")
            .then(response => {
                fillDataUpdate(response.data);
            })
            .then(() => {
                $("#modal-edit").modal("show");
            })
            .finally(() => {
                hideLoader();
            });
    });

    // edit add
    $(document).on("click", "#btn-form-add", function(e) {
        e.preventDefault();
        resetFormAdd();
        removeErrorFormModal();
        $("#modal-add").modal("show");
    });

    // add
    $(document).on("click", "#btn-add-product", function(e) {
        e.preventDefault();
        removeErrorFormModal();
        let url = $("#add-form-product").attr("data-url");
        var formData = new FormData($("#add-form-product")[0]);
        showLoader();
        callAjax(url, "POST", formData)
            .then(() => {
                getListProducts();
                $("#modal-add").modal("hide");
            })
            .catch(err => {
                fillErrorFormModal(err.responseJSON.errors); //ajax setup
            })
            .finally(() => {
                hideLoader();
            });
    });

    // delete
    $(document).on("click", ".btn-delete", function(e) {
        e.preventDefault();
        let url = $(this).attr("data-url");
        let data = { _token: $("#token").val() };
        deleteProduct(url, data);
    });

    async function deleteProduct(url, data) {
        const result = await confirmDeleteProduct();
        if (result.isConfirmed) {
            showLoader();
            const dataDelete = await callAjax(url, "DELETE", data);
            await getListProducts();
            hideLoader();
            alertSuccess(dataDelete.message);
        }
    }

    // images edit
    let imageList = [];
    $(document).on("click", ".btn-edit-images", function(e) {
        e.preventDefault();
        $("#images-form").trigger("reset");
        removeErrorFormModal();
        let urlUpdate = $(this).attr("url-update");
        $(".update-images").attr("data-url", urlUpdate);
        let urlShow = $(this).attr("data-url-show");
        $(".update-images").attr("url-show", urlShow);
        $("#images-form").trigger("reset");
        let url = $(this).attr("data-url-show");
        imageList = [];
        getImagesWithUrl(url);
    });

    // images update
    $(document).on("click", ".update-images", function(e) {
        e.preventDefault();
        removeErrorFormModal();
        let url = $(this).attr("data-url");
        let urlShow = $(this).attr("url-show");
        let formData = new FormData($("#images-form")[0]);
        for (let i = 0; i < imageList.length; i++) {
            formData.append(
                `imageListDelete[${imageList[i].id}]`,
                imageList[i].name
            );
        }
        showLoader();
        callAjax(url, "POST", formData)
            .then(() => {
                getImagesWithUrl(urlShow);
            })
            .catch(err => {
                fillErrorFormImageModal(err.responseJSON.errors);
            })
            .finally(() => {
                $("#images-form").trigger("reset");
                getListProducts();
                hideLoader();
            });
    });

    // delete image
    $(document).on("click", ".btn-delete-image-current", function(e) {
        e.preventDefault();
        let id = $(this).attr("data-id");
        let name = $(this).attr("data-name");
        getListArrayImageDeleteCurrent(id, name);
        $(this)
            .parent()
            .parent()
            .remove();
        removeErrorFormModal();
    });

    $(document).on("click", ".btn-delete-image-new", function(e) {
        e.preventDefault();
        let nameImg = $(this).attr("name-image");
        let listFileInput = $(".file-upload")[0].files;
        $(".file-upload")[0].files = removeFileFromFileListImageNew(
            nameImg,
            listFileInput
        );
        $(this)
            .parent()
            .parent()
            .remove();
    });

    function removeFileFromFileListImageNew(nameImg, listFileInput) {
        const dt = new DataTransfer();
        for (let i = 0; i < Object.keys(listFileInput).length; i++) {
            let { name } = listFileInput[i];
            if (name !== nameImg) dt.items.add(listFileInput[i]);
        }
        return (listFileInput = dt.files);
    }

    function getListArrayImageDeleteCurrent(id, name) {
        let image = { id, name }; //{id: id, name: name}
        let existImage = imageList.some(img => img.id === image.id);
        if (!existImage) imageList.push(image);
    }

    function readURLFileUpload(input) {
        for (var i = 0; i < input.files.length; i++) {
            if (input.files[i]) {
                var reader = new FileReader();
                let name = input.files[i].name;
                reader.onload = function(e) {
                    var images = { images: e.target.result, nameImage: name };
                    var source = $("#upload-template").html();
                    var template = Handlebars.compile(source);
                    $("#images-render").append(template(images));
                };
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
    // upload file
    $(document).on("change", ".file-upload", function() {
        let url = $(".btn-edit-images").attr("data-url-show");
        getImagesWithUrl(url).then(() => {
            readURLFileUpload(this);
        });
    });

    async function getImagesWithUrl(urlShow) {
        showLoader();
        return callAjax(urlShow, "GET")
            .then(response => {
                fillDataUpdateImages(response.data);
            })
            .then(() => {
                $("#modal-images").modal("show");
            })
            .finally(() => {
                hideLoader();
            });
    }

    async function confirmDeleteProduct(title, content) {
        if (title === undefined) title = "Are you sure?";
        if (content === undefined)
            content = "You won't be able to revert this!";
        return Swal.fire({
            title: title,
            text: content,
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        });
    }

    function fillErrorFormImageModal(err) {
        $.each(err, function(key, value) {
            for (const val of value) {
                let name = key.replace(/.[0-9]/, "");
                $(".errors." + name).append(val + "<br>");
            }
        });
    }

    function alertSuccess(data) {
        if (data === undefined) data = "Success";
        Swal.fire({
            title: "Message",
            text: data,
            type: "success",
            icon: "success"
        });
    }

    function resetFormAdd() {
        $("#add-form-product").trigger("reset");
        $("img#output1").attr("src", "");
        $("#output1").css("display", "none");
    }

    function fillDataViewProduct(product, images) {
        $("#price-view").html(product.price);
        $("#modal-category").html(product.namecate.name);
        $("#modal-name").html(product.name);
        $("#description").html(product.description);
        if (images.length === 0) {
            $(".img-modal-row").attr("src", URL_IMAGE + "default.jpg");
        } else {
            $(".img-modal-row").attr("src", URL_IMAGE + images[0].file);
        }
    }

    function fillDataUpdate(response) {
        $("#status")
            .val(response.status)
            .attr("selected", "selected");
        $("#name-product").val(response.name);
        $("#description1").html(response.description);
        $("#price").val(response.price);
        $("#cate-id")
            .val(response.namecate.id)
            .attr("selected", "selected");
    }

    function fillDataUpdateImages(response) {
        var images = { images: response.images };
        var source = $("#download-template").html();
        var template = Handlebars.compile(source);
        $("#images-render").html(template(images));
    }
    //Pagination
    $("body").on("click", ".pagination li a", function(e) {
        e.preventDefault();
        let url = $(this).attr("href");
        showLoader();
        callAjax(url)
            .then(function(response) {
                $("#list-product").html(response);
            })
            .finally(() => {
                hideLoader();
            });
    });

    //reder image
    // $(() => {
    //     $("#avatar-add").on('change', function(e) {
    //         e.preventDefault()
    //         $('#output1').css('display', 'block')
    //         var reader = new FileReader()
    //         reader.onload = (e) => {
    //             document.getElementById("output1").src = e.target.result
    //         }
    //         reader.readAsDataURL(this.files[0])
    //     })

    //     //
    //     $("#avatar-edit").on('change', function(e) {
    //         e.preventDefault()
    //         $('#output').css('display', 'block')
    //         var reader = new FileReader()
    //         reader.onload = (e) => {
    //             document.getElementById("output").src = e.target.result
    //         }
    //         reader.readAsDataURL(this.files[0])
    //     })
    // })

    let searchProduct = debounce(function() {
        let url = $("#form-product-search").attr("data-url");
        let dataForm = $("#form-product-search").serialize();
        showLoader();
        callAjax(url, "GET", dataForm)
            .then(data => {
                $("#list-product").html(data);
            })
            .finally(() => {
                hideLoader();
            });
    }, TIME_SEARCH);

    $(document).on("keyup", "#search-product", searchProduct);
    $(document).on("change", "#search-category", searchProduct);
});
