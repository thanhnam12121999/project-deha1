$(document).ready(function() {

    const TIME_SEARCH = 1000;

    async function getListUser() {
        let url = $('#data-user').attr('data-url');
        return callAjax(url, 'GET')
            .then((data) => {
                $('#list-user').html(data);
            });
    }
    getListUser()

    // update
    $(document).on('click', '#update-user', function(e) {
        e.preventDefault();
        let url = $(this).attr('data-url');
        let formData = new FormData($('#editFormUser')[0]);
        removeErrorFormModal()
        callAjax(url, 'POST', formData)
            .then(function() {
                getListUser()
                $('#modal-user-edit').modal('hide');
            })
            .catch(function(err) {
                fillErrorFormModal(err.responseJSON.errors)
            })
    })

    // edit
    $(document).on('click', '.btn-update-user', function(e) {
        e.preventDefault();
        $('#editFormUser').trigger("reset")
        $('#update-user').attr('data-url', $(this).attr('data-action'));
        removeErrorFormModal()
        let url = $(this).attr('data-url-show')
        callAjax(url, "GET")
            .then((data) => {
                fillUpdateDataUser(data)
            })
            .then(() => {
                $('#modal-user-edit').modal('show')
            })
    })

    // Edit add
    $(document).on('click', '#btn-add-user', function(e) {
        e.preventDefault();
        $('#add-form-user').trigger("reset")
        $('.select2-selection__rendered').html('')
        removeErrorFormModal()
        $('#modal-user-add').modal('show')
    })

    // add
    $(document).on('click', '#btn-save-user', function(e) {
        e.preventDefault();
        let url = $(this).attr('data-url');
        var formData = new FormData($('#add-form-user')[0])
        removeErrorFormModal()
        callAjax(url, "POST", formData)
            .then(function() {
                getListUser()
                $('#modal-user-add').modal('hide')
            })
            .catch(function(err) {
                fillErrorFormModal(err.responseJSON.errors)
            })
    })

    $(document).on('click', '.btn-delete-user', function(e) {
        e.preventDefault();
        let url = $(this).attr('data-url');
        var data = { "_token": $('#token').val() };
        async function deleteUser() {
            const result = await swalFireConfirmData()
            if (result.isConfirmed) {
                const dataDelete = await callAjax(url, 'DELETE', data)
                await getListUser()
                deleteUserSuccess(dataDelete)
            }
        }
        deleteUser()
    })

    async function swalFireConfirmData() {
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
        })
    }

    function deleteUserSuccess() {
        Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
        )
    }

    function fillUpdateDataUser(response) {
        $('#role_id option').select2().removeAttr('selected')
        let role = response.data.roles;
        $('#name-user-edit').val(response.data.name)
        $('#username-edit').val(response.data.username)
        $('.phone-edit').val(response.data.phone)
        for (const key in role) {
            $('#role_id').val(role[key].id).attr('selected', 'selected')
            $('#role_id').val(role[key].id).trigger('change')
        }
    }

    //Pagination
    $('body').on('click', '.pagination li a', function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        callAjax(url)
            .then(function(response) {
                $('#list-user').html(response);
            })
    });

    // search
    let searchUser = debounce(function() {
        let url = $(this).attr('data-url');
        let dataForm = $('#form-user-search').serialize();
        callAjax(url, 'GET', dataForm)
            .then((data) => {
                $('#list-user').html(data);
            })
    }, TIME_SEARCH);

    $(document).on('keyup', '#search-user', searchUser);

    //select2 
    $('.slect2_init').select2({
        'placeholder': 'Pick Role'
    })
});