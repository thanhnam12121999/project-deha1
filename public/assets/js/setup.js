$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})

async function callAjax(url, method = 'GET', data = '') {

    return $.ajax({
        type: method,
        url: url,
        data: data,
        processData: false,
        contentType: false,//
    });
}

function showLoader() {
    $('.loader').css('display', 'block')
}

function hideLoader() {
    $('.loader').css('display', 'none')
}

function removeErrorFormModal() {
    $('.errors').text('')
}

function fillErrorFormModal(err) {
    $('.modal-body-main').scrollTop(0)
    $.each(err, function(key, value) {
        $('.errors.'+key).html(value) // errors-name
    })
}

function debounce(func, wait) {
    let timeout;
    return function() {
        let context = this,
            args = arguments;
        let executeFunction = function() {
            func.apply(context, args);
        };
        clearTimeout(timeout);
        timeout = setTimeout(executeFunction, wait);
    };
}