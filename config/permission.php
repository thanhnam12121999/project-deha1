<?php
    return [
        // access permission
        // 'access' => [
        //     'product-list' => 'product_list',
        //     'product-show' => 'product_show',
        //     'product-add' => 'product_add',
        //     'product-delete' => 'product_delete',
        //     'product-edit' => 'product_edit',
        //     'role-list' => 'role_list',
        //     'role-show' => 'role_show',
        //     'role-add' => 'role_add',
        //     'role-edit' => 'role_edit',
        //     'role-delete' => 'role_delete',
        //     'user-list' => 'user_list',
        //     'user-show' => 'user_show',
        //     'user-add' => 'user_add',
        //     'user-edit' => 'user_edit',
        //     'user-delete' => 'user_delete',
        //     'category-list' => 'category_list',
        //     'category-show' => 'category_show',
        //     'category-add' => 'category_add',
        //     'category-edit' => 'category_edit',
        //     'category-delete' => 'category_delete',
        //     'permission-list' => 'permission_list',
        //     'permission-add' => 'permission_add',
        // ],

        // permission seeder
        'permissions' => [
            'product' => ['list','show', 'add', 'edit', 'delete', 'upload'],
            'role' => ['list','show', 'add', 'edit', 'delete'],
            'user' => ['list','show', 'add', 'edit', 'delete'],
            'permission' => ['list','add'],
            'category' => ['list','show', 'add', 'edit', 'delete'],
        ],
        // add permission
        'permission_childrent' => ['list','show', 'add', 'edit', 'delete', 'download'],
        'permission_parents' => ['product', 'role', 'user', 'permission', 'category'],
    ];
